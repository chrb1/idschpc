#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 16:27:31 2020

@author: christophe
"""

from math import pi, sin, cos
from matplotlib import pyplot as plt
import numpy as np
import scipy as sp
import scipy.sparse as spa
import scipy.linalg as la
#from matplotlib import rc, rcParams
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
#rc('text', usetex=True)
#rcParams.update({'font.size': 16})

# Some paramters
_eps =1e-12
_maxiter=500

def _basic_check(A, b, x0):
    """ Common check for clarity """
    n, m = A.shape
    if(n != m):
        raise ValueError("Only square matrix allowed")
    if(b.size != n):
        raise ValueError("Bad rhs size")
    if (x0 is None):
        x0 = np.zeros(n)
    if(x0.size != n):
        raise ValueError("Bad initial value size")
    return x0


def laplace(n):
    """ Construct the 1D laplace operator """
    A = np.array(n,n)
    # TODO: Some work here
    # ...
    # ...
    return A


def JOR(A, b, x0=None, omega=0.5, eps=_eps, maxiter=_maxiter):
    """
    Solve A x = b
    - A is the sparse matrix
    - b is the right hand side (np.array)
    - omega is the relaxation parameter
    - eps is the convergence threshold
    - maxiter limits the number of iterations

    Methode itérative stationnaire de sur-relaxation (Jacobi over relaxation)
    Convergence garantie si A est à diagonale dominante stricte
    A = D - E - F avec D diagonale, E (F) tri inf. (sup.) stricte
    Le préconditionneur est diagonal M = (1./omega) * D


    Output:
        - x is the solution at convergence or after maxiter iteration
        - residual_history is the norm of all residuals
    """
    x = _basic_check(A, b, x0)
    r = np.zeros(x.shape)
    residual_history = list()

    # TODO: Some work here
    # ...
    # ...
    return x, residual_history


def SOR(A, b, x0=None, omega=1.5, eps=_eps, maxiter=_maxiter):
    """
    Solve A x = b where
    - A is the sparse matrix
    - b is the right hand side (np.array)
    - omega is the relaxation parameter
    - eps is the convergence threshold
    - maxiter limits the number of iterations

    Methode itérative stationnaire de sur-relaxation successive
    (Successive Over Relaxation)

    A = D - E - F avec D diagonale, E (F) tri inf. (sup.) stricte
    Le préconditionneur est tri. inf. M = (1./omega) * D - E

    * Divergence garantie pour omega <= 0. ou omega >= 2.0
    * Convergence garantie si A est symétrique définie positive pour
    0 < omega  < 2.
    * Convergence garantie si A est à diagonale dominante stricte pour
    0 < omega  <= 1.

    Output:
        - x is the solution at convergence or after maxiter iteration
        - residual_history is the norm of all residuals

    """
    if (omega > 2.) or (omega < 0.):
        raise ArithmeticError("SOR will diverge")

    x = _basic_check(A, b, x0)
    r = np.zeros(x.shape)
    residual_history = list()

    # TODO: Some work here
    # ...
    # ...
    return x, residual_history


def injection(fine, coarse):
    """
    Classical injection, that only keep the value of the coarse nodes
    The modification of coarse is done inplace
    """
    # TODO: Some work here
    # ...
    # ...

    return coarse

def interpolation(coarse, fine):
    """
    Classical linear interpolation (the modification of fine is done inplace)
    """
    # TODO: Some work here
    # ...
    # ...

    return fine


def plot(x, y, custom, label=""):
    """
    A custom plot function, usage:
        f, ax = plot(x, y,'-x', label="u")
    """
    f = plt.figure()
    ax = f.add_subplot(111)
    ax.plot(x, y, custom, label=label);
    ax.set_xlabel(r"x")
    ax.set_ylabel(r"u(x)")
    f.tight_layout()
    return f, ax

def tgcyc(nsegment=64, engine=JOR, **kwargs):
    """
    Two grid cycle:
        - nsegment: the number of segment so that h = 1.0/nsegment
        - engine: the stationary iterative method used for smoothing

    Warning: make the good distinction between the number of segments, the
    number of nodes and the number of unknowns
    """
    if(nsegment%2): raise ValueError("nsegment must be even")

    # Beware that : nsegment
    # n = number of nodes
    # n_inc = number of unknowns
    n = nsegment + 1
    h = 1.0 / nsegment
    H = 2. * h

    # n_inc_h = ? # TODO
    # n_inc_H = ? # TODO

    # Full points
    xh = np.linspace(0.,1., n)
    xH = np.linspace(0.,1., n//2 + 1)
    # Inner points
    xih = xh[1:-1]
    xiH = xH[1:-1]

    # construction of Laplace operator
    # Ah = ? # TODO
    # AH = ? # TODO

    # Initial guess
    u0 = 0.5 * (np.sin(16. * xih * pi) + np.sin(40. * xih * pi))
    u = np.zeros(u0.shape)

    # The given right hand side (null for now)
    f = np.zeros(u.shape)

    # Pre-smoothing Relaxation

    # Restriction with injection

    # Solve on the coarse grid

    # Prolongation

    # Update solution

    # Post-smoothing Relaxation

    return u


# For debugging:
tgcyc(nsegment=8)

# For real application
# tgcyc(nsegment=64)